/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.bookmarker;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {
  private MainMotor motor;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    motor = ViewModelProviders.of(this).get(MainMotor.class);

    saveBookmark(getIntent());

    final RecyclerView rv = findViewById(R.id.bookmarks);
    final BookmarkAdapter adapter = new BookmarkAdapter(getLayoutInflater());

    rv.setLayoutManager(new LinearLayoutManager(this));
    rv.addItemDecoration(new DividerItemDecoration(this,
      DividerItemDecoration.VERTICAL));
    rv.setAdapter(adapter);

    motor.states.observe(this, state -> {
      adapter.submitList(state.content);
    });

    motor.getSaveEvents().observe(this, event -> event.handle(result -> {
      String message;

      if (result.error == null) {
        message = result.content.title + " was saved!";
      }
      else {
        message = result.error.getLocalizedMessage();
      }

      Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
    }));
  }

  private void saveBookmark(Intent intent) {
    if (Intent.ACTION_SEND.equals(intent.getAction())) {
      String pageUrl = getIntent().getStringExtra(Intent.EXTRA_STREAM);

      if (pageUrl == null) {
        pageUrl = getIntent().getStringExtra(Intent.EXTRA_TEXT);
      }

      if (pageUrl != null &&
        Uri.parse(pageUrl).getScheme().startsWith("http")) {
        motor.save(pageUrl);
      }
      else {
        Toast.makeText(this, R.string.msg_invalid_url,
          Toast.LENGTH_LONG).show();
        finish();
      }
    }
  }
}
