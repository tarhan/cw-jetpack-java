/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.pdfprovider;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProviders;

public class MainActivity extends AppCompatActivity {
  private static final String AUTHORITY =
    BuildConfig.APPLICATION_ID + ".provider";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    final MainMotor motor = ViewModelProviders.of(this).get(MainMotor.class);
    final View export = findViewById(R.id.export);
    final View view = findViewById(R.id.view);
    final TextView error = findViewById(R.id.error);

    motor.getStates().observe(this, state -> {
      export.setEnabled(!state.isLoading && state.content == null);
      view.setEnabled(!state.isLoading && state.content != null);

      if (view.isEnabled()) {
        view.setOnClickListener(v -> {
          Uri uri = FileProvider.getUriForFile(this, AUTHORITY, state.content);
          Intent intent =
            new Intent(Intent.ACTION_VIEW)
              .setDataAndType(uri, "application/pdf")
              .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

          startActivity(intent);
        });
      }

      if (state.error != null) {
        error.setText(state.error.getLocalizedMessage());
      }
    });

    export.setOnClickListener(v -> motor.exportPdf());

  }
}
