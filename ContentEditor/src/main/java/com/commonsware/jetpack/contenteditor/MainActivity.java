/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.contenteditor;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

public class MainActivity extends AppCompatActivity {
  private static final String FILENAME = "test.txt";
  private static final int REQUEST_SAF = 1337;
  private static final int REQUEST_PERMS = 123;
  private MainMotor motor;
  private Uri current;
  private EditText text;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    final View progress = findViewById(R.id.progress);
    final TextView title = findViewById(R.id.title);

    text = findViewById(R.id.text);
    motor = ViewModelProviders.of(this).get(MainMotor.class);

    motor.getResults().observe(this, result -> {
      current = result.source;

      progress.setVisibility(result.isLoading ? View.VISIBLE : View.GONE);
      text.setEnabled(!result.isLoading);

      if (result.source == null) {
        title.setText("");
      }
      else {
        title.setText(result.source.toString());
      }

      if (TextUtils.isEmpty(text.getText())) {
        text.setText(result.text);
      }

      if (result.error != null) {
        text.setText(result.error.getLocalizedMessage());
        text.setEnabled(false);
        Log.e("ContentEditor", "Exception in I/O", result.error);
      }
    });

    loadFromDir(getFilesDir());
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.actions, menu);

    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.loadInternal:
        loadFromDir(getFilesDir());
        return true;

      case R.id.loadExternal:
        loadFromDir(getExternalFilesDir(null));
        return true;

      case R.id.loadExternalRoot:
        loadFromExternalRoot();
        return true;

      case R.id.openDoc:
        startActivityForResult(
          new Intent(Intent.ACTION_OPEN_DOCUMENT)
            .setType("text/*")
            .addCategory(Intent.CATEGORY_OPENABLE), REQUEST_SAF);
        return true;

      case R.id.newDoc:
        startActivityForResult(
          new Intent(Intent.ACTION_CREATE_DOCUMENT)
            .setType("text/plain")
            .addCategory(Intent.CATEGORY_OPENABLE), REQUEST_SAF);
        return true;

      case R.id.save:
        motor.write(current, text.getText().toString());
        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode,
                                  @Nullable Intent data) {
    if (requestCode == REQUEST_SAF) {
      if (resultCode == RESULT_OK && data != null && data.getData() != null) {
        text.setText("");
        motor.read(data.getData());
      }
    }
    else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
                                         @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    if (requestCode == REQUEST_PERMS) {
      if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        loadFromDir(Environment.getExternalStorageDirectory());
      }
      else {
        Toast.makeText(this, R.string.msg_sorry, Toast.LENGTH_LONG).show();
      }
    }
  }

  private void loadFromDir(File dir) {
    text.setText("");
    motor.read(Uri.fromFile(new File(dir, FILENAME)));
  }

  private void loadFromExternalRoot() {
    if (ContextCompat.checkSelfPermission(this,
      Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
      PackageManager.PERMISSION_GRANTED) {
      loadFromDir(Environment.getExternalStorageDirectory());
    }
    else {
      String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

      ActivityCompat.requestPermissions(this, perms, REQUEST_PERMS);
    }
  }
}
