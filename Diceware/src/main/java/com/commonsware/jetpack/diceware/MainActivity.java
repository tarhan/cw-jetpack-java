/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.diceware;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

public class MainActivity extends AppCompatActivity {
  private static final int REQUEST_OPEN = 1337;
  private MainMotor motor;
  private int wordCount = 6;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    motor = ViewModelProviders.of(this).get(MainMotor.class);

    final View progress = findViewById(R.id.progress);
    final TextView passphrase = findViewById(R.id.passphrase);

    motor.viewStates.observe(this, viewState -> {
      progress.setVisibility(
        viewState.isLoading ? View.VISIBLE : View.GONE);

      if (viewState.content != null) {
        passphrase.setText(viewState.content);
      }
      else if (viewState.error != null) {
        passphrase.setText(viewState.error.getLocalizedMessage());
        Log.e("Diceware", "Exception generating passphrase",
          viewState.error);
      }
      else {
        passphrase.setText("");
      }
    });

    motor.generatePassphrase(wordCount);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.actions, menu);

    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.open:
        Intent i =
          new Intent()
            .setType("text/plain")
            .setAction(Intent.ACTION_OPEN_DOCUMENT)
            .addCategory(Intent.CATEGORY_OPENABLE);

        startActivityForResult(i, REQUEST_OPEN);
        return true;

      case R.id.refresh:
        motor.generatePassphrase(wordCount);
        return true;

      case R.id.word_count_4:
      case R.id.word_count_5:
      case R.id.word_count_6:
      case R.id.word_count_7:
      case R.id.word_count_8:
      case R.id.word_count_9:
      case R.id.word_count_10:
        item.setChecked(!item.isChecked());

        int temp = Integer.parseInt(item.getTitle().toString());

        if (temp != wordCount) {
          wordCount = temp;
          motor.generatePassphrase(wordCount);
        }

        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode,
                                  @Nullable Intent data) {
    if (requestCode == REQUEST_OPEN) {
      if (resultCode == RESULT_OK && data != null) {
        motor.generatePassphrase(data.getData(), wordCount);
      }
    }
  }
}
