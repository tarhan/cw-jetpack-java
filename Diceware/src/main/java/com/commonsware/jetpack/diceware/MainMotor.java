/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.diceware;

import android.app.Application;
import android.net.Uri;
import android.text.TextUtils;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

public class MainMotor extends AndroidViewModel {
  private final PassphraseRepository repo;
  private final Executor executor = Executors.newSingleThreadExecutor();
  private Uri wordsDoc = PassphraseRepository.ASSET_URI;
  final MutableLiveData<MainViewState> viewStates =
    new MutableLiveData<>();

  public MainMotor(@NonNull Application application) {
    super(application);

    repo = PassphraseRepository.get(application);
  }

  void generatePassphrase(int wordCount) {
    viewStates.setValue(new MainViewState(true, null, null));

    executor.execute(() -> {
      try {
        viewStates.postValue(new MainViewState(false,
          TextUtils.join(" ", repo.generate(wordsDoc, wordCount)), null));
      }
      catch (Exception e) {
        viewStates.postValue(new MainViewState(false, null, e));
      }
    });
  }

  void generatePassphrase(Uri wordsDoc, int wordCount) {
    this.wordsDoc = wordsDoc;

    generatePassphrase(wordCount);
  }
}
