/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.fragments;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

class ToDoRepository {
  static final ToDoRepository INSTANCE = new ToDoRepository();
  private final Map<String, ToDoModel> items = new HashMap<>();

  private ToDoRepository() {
    add(new ToDoModel(UUID.randomUUID().toString(),
      "Buy a copy of _Elements of Android Jetpack_", true,
      "See https://wares.commonsware.com",
      Calendar.getInstance()));
    add(
      new ToDoModel(UUID.randomUUID().toString(), "Read the entire book",
        false, null,
        Calendar.getInstance()));
    add(new ToDoModel(UUID.randomUUID().toString(),
      "Write an app for somebody in my community", false,
      "Talk to some people at non-profit organizations to see what they need!",
      Calendar.getInstance()));
  }

  @NonNull
  List<ToDoModel> getItems() {
    return new ArrayList<>(items.values());
  }

  @Nullable
  ToDoModel findItemById(String id) {
    return items.get(id);
  }

  private void add(ToDoModel model) {
    items.put(model.id, model);
  }
}
